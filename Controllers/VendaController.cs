using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;


namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Create(Venda venda)
        {
            var vendaDB = new Entities.Venda
            {
                Date = venda.Date,
                Vendedor = venda.Vendedor,
                Itens = venda.Itens
            };
            _context.Vendas.Add(venda);
            _context.Vendedor.Add(venda.Vendedor);

            foreach(var item in venda.Itens)
            {
                _context.Item.Add(item);
            }
            
            _context.SaveChanges();
            
            return Ok(venda);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var vendaDB = _context.Vendas.Where(v => v.Id == id).Select(v => new { 
                    id = v.Id,
                    data = v.Date,
                    vendedor = v.Vendedor,
                    itens = v.Itens,
                    status = v.Status
            });

            if(vendaDB is null)
                return NotFound();
            
            return Ok(vendaDB);
        }


        [HttpPut("{id}")]
        public IActionResult UpdateVenda(int id, Status status)
        {
            var vendaDB = _context.Vendas.Find(id);

            if(vendaDB is null)
                return NotFound();

            if(vendaDB.Status.Equals(Status.AguardandoPagamento) && status.ToString().Equals("PagamentoAprovado") || 
                vendaDB.Status.Equals(Status.AguardandoPagamento) && status.ToString().Equals("Cancelada"))
            {
                vendaDB.Status = status;
            }

            else if(vendaDB.Status.Equals(Status.PagamentoAprovado) && status.ToString().Equals("EnviadoParaTransportadora") || 
                vendaDB.Status.Equals(Status.PagamentoAprovado) && status.ToString().Equals("Cancelada"))
            {
                vendaDB.Status = status;
            }

            else if(vendaDB.Status.Equals(Status.EnviadoParaTransportadora) && 
                status.ToString().Equals("Entregue"))
            {
                vendaDB.Status = status;
            }

            else
              return BadRequest(new { mensagem = "Atualização do Status: Aguardando Pagamento > Pagamento Aprovado ou Cancelado " + 
                                        "- Pagamento Aprovado > Enviado para Transportadora ou Cancelado" + 
                                        "- Enviado para Transportadora > Entregue"});

            
            _context.Vendas.Update(vendaDB);
            _context.SaveChanges();
            return Ok(new { vendaDB.Id, vendaDB.Date, vendaDB.Status });
        }

    }
}