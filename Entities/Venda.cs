using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public Atendente Vendedor { get; set; }
        public List<Item> Itens { get; set; }
        [DefaultValue(0)]
        public Status Status { get; set; }

    }
}