namespace tech_test_payment_api.Entities
{
    public enum Status
    {
        AguardandoPagamento = 0,
        PagamentoAprovado = 1,
        EnviadoParaTransportadora = 2,
        Entregue = 3,
        Cancelada = 4
    }
}